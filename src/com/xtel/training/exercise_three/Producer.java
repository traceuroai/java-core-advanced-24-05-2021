package com.xtel.training.exercise_three;

import java.util.LinkedList;

public class Producer extends Thread {

    public static LinkedList<String> Queue = new LinkedList<String>();

//    public static boolean flag = true;

    @Override
    public void run() {

        synchronized (this)
        {
            String str = String.valueOf(Math.random());
            System.out.println("value random " + str);
            Queue.offer(str);
            if(Queue.size() == 5)
            {
                System.out.println("Full Queue");
                try {
                    System.out.println("waiting");
                    wait(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
