package com.xtel.training.exercise_three;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Consummer extends Thread{

    @Override
    public void run() {

        synchronized (this)
        {
            if(Producer.Queue.isEmpty())
            {
                try {
                    wait(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println( "peek value " + Producer.Queue.peek());
            Producer.Queue.pop();

        }



    }
}
