package com.xtel.training.exercise_four;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import org.apache.log4j.Logger;

public class ServerSocket {

    private static final Logger logger = Logger.getLogger(ServerSocket.class);

    public static void main(String[] args) {

        java.net.ServerSocket serverSocket = null;
        String message = null;
        BufferedReader bufferedReader;
        BufferedWriter bufferedWriter;
        Socket socketOfProducer = null;
        String line = null;

        try {
            serverSocket = new java.net.ServerSocket(9999);
        } catch (IOException e) {
            logger.error("Lỗi không thể tạo Server ");
            System.out.println(e);
            System.exit(1);
        }


        try {
            System.out.println("Server is waiting to accept user...");


            // Chấp nhận một yêu cầu kết nối từ phía Client.
            // Đồng thời nhận được một đối tượng Socket tại server.

            socketOfProducer = serverSocket.accept();
            System.out.println("Accept a client!");
            System.out.println("waiting ...");


            // Mở luồng vào ra trên Socket tại Server.
            bufferedReader = new BufferedReader(new InputStreamReader(socketOfProducer.getInputStream()));
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socketOfProducer.getOutputStream()));


                message = bufferedReader.readLine();
                System.out.println("client : " + message);

                System.out.println(message);


                    Scanner sc = new Scanner(System.in);
                    System.out.println("repply : " );
                    line = sc.nextLine();

                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                    bufferedWriter.flush();



        } catch (IOException e) {

            System.out.println(e);
            e.printStackTrace();
        }
        System.out.println("Sever stopped!");
    }


}
