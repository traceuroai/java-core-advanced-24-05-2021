package com.xtel.training.exercise_four;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Scanner;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class ClientSocket {

    private static final Logger logger = Logger.getLogger(ClientSocket.class);

    public static void main(String[] args) {

        // Địa chỉ máy chủ.
        final String serverHost = "localhost";

        Socket socketOfClient = null;
        BufferedWriter os = null;
        BufferedReader is = null;
        String line = null;

        try {
            socketOfClient = new Socket(serverHost, 9999);

            os = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));

            is = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + serverHost);

            return;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " + serverHost);

            // log4j
            BasicConfigurator.configure();
            logger.warn(new Date());

            return;
        }

        try {

            Scanner sc = new Scanner(System.in);
            System.out.println("repply : ");
            line = sc.nextLine();
            os.write(line);
            os.newLine();
            os.flush();

            System.out.println("waiting ...");

            String responseLine;
            responseLine = is.readLine();
            if(responseLine != null)
            {
                System.out.println("Server: " + responseLine);
            }

            os.close();
            is.close();
            socketOfClient.close();
        } catch (UnknownHostException e) {
            System.err.println("Trying to connect to unknown host: " + e);
        } catch (IOException e) {
            System.err.println("IOException:  " + e);
        }



    }

}
