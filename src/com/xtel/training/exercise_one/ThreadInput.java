package com.xtel.training.exercise_one;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.PrintWriter;
import java.util.Scanner;

@Data
@AllArgsConstructor
public class ThreadInput extends Thread{

    ThreadWriteFile threadWriteFile;
    @Override
    public void run() {
        while (true)
        {
            Scanner sc = new Scanner(System.in);
            String str;
            System.out.println("nhập vào stop để dừng ");
            str = sc.nextLine();
            if(str.trim().equals("stop"))
            {
                threadWriteFile.getPrintWriter().close();
                threadWriteFile.stopping();
                return;
            }
        }

    }
}
