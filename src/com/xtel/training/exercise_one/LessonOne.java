package com.xtel.training.exercise_one;
import java.io.FileNotFoundException;

/* Viết 1 chương trình chạy real-time,
ghi các số nguyên random ra file output.txt .
Dừng chương trình khi gõ lệnh stop trên cửa sổ chương trình. */


public class LessonOne{

    public static void main(String[] args) throws FileNotFoundException {

        ThreadWriteFile threadWriteFile = new ThreadWriteFile();
        threadWriteFile.start();
        ThreadInput threadReadConsole = new ThreadInput(threadWriteFile);
        threadReadConsole.start();

    }

}
