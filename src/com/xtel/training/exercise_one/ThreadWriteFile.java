package com.xtel.training.exercise_one;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

@Data
public class ThreadWriteFile extends Thread {

    private File file;
    private FileOutputStream fileOutputStream;
    private PrintWriter printWriter;

    public static boolean flag = true;

    @Override
    public void run() {

        file  = new File("output.txt");
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        printWriter = new PrintWriter(fileOutputStream);

        while (flag)
        {
            printWriter.print( (int) (Math.random() * 100) + " ");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }

    public static void stopping()
    {
        flag = false;
    }

}
