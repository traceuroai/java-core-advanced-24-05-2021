package com.xtel.training.example_queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExamplePriorityQueue {

    public static void main(String[] args) {

        // su dung queue : phan tu se duoc sap xep theo vi tri tu nhien cua chung.
        // sap xep theo thu tu alphabet(A-Z)
        Queue<String> queue = new PriorityQueue<String>();

        // them phan tu : offer : thanh cong (true), hang doi khong con cho (false)
        queue.offer("N");
        queue.offer("A");
        queue.offer("M");
        queue.offer("E");

        while (true)
        {
            // lay ra va loai bo phan tu dau tien :
            // tra ve null neu hang doi khong co phan tu.
            String name = queue.poll();
            if(name == null)
            {
                break;
            }
            System.out.println("name : " + name);

        }

        // su dung Queue voi kieu nguoi dng tu dinh nghia :

        Queue<Book> queueBook = new PriorityQueue<Book>();

        Book b1 = new Book(111, "java", "author1", "publisher1", 3);
        Book b2 = new Book(121, "c", "author2", "publisher2", 6);
        Book b3 = new Book(101, "android", "author3", "publisher3", 10);

        queueBook.add(b1);
        queueBook.add(b2);
        queueBook.add(b3);

        System.out.println(" cac phan tu cua queueBook : ");
        for(Book book : queueBook)
        {
            System.out.println(book);
        }

        System.out.println(" sau khi xoa : ");
        System.out.println(queueBook.poll());
        System.out.println(queueBook.poll());
        System.out.println(queueBook.poll());


    }

}
