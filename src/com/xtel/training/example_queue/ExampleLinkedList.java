package com.xtel.training.example_queue;

import java.util.LinkedList;

public class ExampleLinkedList {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<String>();
        list.add("java");
        list.add("C#");
        list.add("PHP");
        list.add("java");

        System.out.println("Các phần tử trong list là : ");
        System.out.println(list);

        // duyệt list sủ dụng for :
        for (String str : list)
        {
            System.out.println(str);
        }

        System.out.println("Phan tu list ban dau : " + list);
        System.out.println("so phan tu cuar list ban dau : " + list.size());

        list.remove();
        System.out.println("Phan tu list sau khi xoa : " + list);
        System.out.println("so phan tu cua list sau khi xoa : " + list.size());

        list.remove(1);
        System.out.println("Phan tu cua list sau khi xoa phan tu co index = 1 : " + list);
        System.out.println("so luong phan tu sau khi xoa : " + list.size());

        // kiem tra xem phan tu co trong list hay khong ?
        System.out.println(list.contains("java"));
        System.out.println(list.contains("Android"));

        // tim kiem phan tu trong list :
        System.out.println(list.indexOf("java"));
        System.out.println(list.indexOf("adnroid"));

    }

}
