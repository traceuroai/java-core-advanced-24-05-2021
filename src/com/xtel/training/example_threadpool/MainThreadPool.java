package com.xtel.training.example_threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainThreadPool {

    public static void main(String[] args) {
        // tao 1 pool chua 5 thread :

        ExecutorService executorService = Executors.newFixedThreadPool(5);

        for(int i = 0; i < 10; i++)
        {
            Runnable worker = new WorkerThread("" + i);

            executorService.execute(worker);
        }
        executorService.shutdown();
        while (!executorService.isTerminated())
        {

        }

        System.out.println("Finisher all threads");

    }

}
