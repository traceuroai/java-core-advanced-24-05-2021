package com.xtel.training.exercise_two;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ThreadRandom extends Thread{

    private int minute;

    @Override
    public void run() {

        for (int i = 0; i < minute * 60; i++)
        {
            try {
                ThreadRandom.sleep(minute * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println((int)(Math.random() * 100));
        }

    }


}
