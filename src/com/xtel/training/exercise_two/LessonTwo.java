package com.xtel.training.exercise_two;

/*b.	Viết 1 chương trình chạy real-time,
cứ n giây in 1 số nguyên random ra mà hình .
Dừng chương trình sau n phút.*/

import java.util.Scanner;

public class LessonTwo{

    public static void main(String[] args) {

        String str;
        int minute;
        int count = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap vao so phut : ");
        str = sc.nextLine();
        if(str.trim().equals(""))
        {
            System.out.println("ban nhap chuoi rong ! ");
            System.exit(0);
        }

        try
        {
            minute = Integer.parseInt(str);
            if(minute < 0)
            {
                System.out.println("so phut khong the < 0");
                System.exit(0);
            }

            ThreadRandom threadRandom = new ThreadRandom(minute);
            threadRandom.run();



        }catch (NumberFormatException e)
        {
            System.out.println("chuoi ban nhap khong hop le ");
            System.exit(0);
        }




    }

}
