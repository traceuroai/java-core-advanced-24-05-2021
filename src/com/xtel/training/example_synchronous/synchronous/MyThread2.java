package com.xtel.training.example_synchronous.synchronous;

public class MyThread2 extends Thread{

    Table1 table1;

    public MyThread2(Table1 table1) {
        this.table1 = table1;
    }

    @Override
    public void run() {
        table1.printTable(100);
    }

}
