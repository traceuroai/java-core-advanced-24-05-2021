package com.xtel.training.example_synchronous.synchronous;

public class MyThread extends Thread{

    Table1 table1;

    public MyThread(Table1 table1) {
        this.table1 = table1;
    }

    @Override
    public void run() {
        table1.printTable(5);
    }

}
