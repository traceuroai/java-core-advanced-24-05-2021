package com.xtel.training.example_synchronous.synchronous;
public class ExampleSynchronized {

    public static void main(String[] args) {
        Table1 table1 = new Table1();
        MyThread myThread = new MyThread(table1);
        MyThread2 myThread2 = new MyThread2(table1);
        myThread.run();
        myThread2.run();
    }

}
