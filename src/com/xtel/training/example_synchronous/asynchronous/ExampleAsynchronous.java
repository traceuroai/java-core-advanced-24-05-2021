package com.xtel.training.example_synchronous.asynchronous;



public class ExampleAsynchronous {

    public static void main(String[] args) {

        // tao mot table :
        Table table = new Table();
        // tao 2 thread
        Thread1 thread1 = new Thread1(table);
        Thread2 thread2 = new Thread2(table);
        thread1.start();
        thread2.start();

    }

}
